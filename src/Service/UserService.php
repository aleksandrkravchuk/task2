<?php

namespace Sample\Service;

use PDOException;
use Redis;
use Sample\Constant;
use Sample\Exception\BookCreationException;
use Sample\Repository\UserRepository;

class UserService
{
    /**
     * Insert one row to DB.
     *
     * @param string $name
     * @param string $email
     * @param string $password
     */
    public function addUser(string $name, string $email, string $password): void
    {

        $redis = new Redis();
        $redis->connect(
            'redis',
            6379
        );

        $redis->publish(
            'eustatos',
            json_encode([
                'test' => 'success'
            ])
        );
        $redis->close();

    }

}